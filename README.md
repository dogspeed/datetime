# Datetime
> Protocol buffer datetime

This protobuff file implements a protocol buffer service of a datetime.

## Messages

* Empty: empty message for input
* Time:
	* epoch: int64 defining epoch time (UNIX time)

## Services

* Datetime:
	* EpochTime (Empty) returns (Time): returns the epoch time (UNIX time)

